using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Iron_E.Reef.IO.Async;
using Iron_E.Reef.Files.VCS;

namespace Iron_E.Reef.Files.Filtering
{
	/// <summary>
	/// A list of <see cref="FileInfo"/>s that have been selected from <b>a</b> root directory and filtered.
	/// </summary>
	public class FilteredFileList: List<FileInfo>
	{
		#region ConstructorArguments
		/// <summary>
		/// Data class for arguments of the <see cref="CreateAsync"/> constructor.
		/// </summary>
		/* TODO: Make this a Record when C# 9 releases. <2020-06-04, Iron_E> */
		public class ConstructorArguments
		{ // †
			#region Properties
			/// <summary>
			/// The path that was targeted for refactoring.
			/// </summary>
			public string Path { get; }

			/// <summary>
			/// If <c>true</c>, skip searching <see cref="Path"/> for a <c>.gitignore</c> file to exclude matches for.
			/// </summary>
			public bool IncludeGitIgnore { get; }

			/// <summary>
			/// A set of <see cref="EnumerationOptions"/> used to control how the <see cref="FilteredFileList"/> are generated.
			/// </summary>
			public EnumerationOptions? FileDiscoveryOptions { get; }

			/// <summary>
			/// A regular expression and behavior to filter the files.
			/// </summary>
			public (FilterType Type, string Expression)? Filter { get; }

			/// <summary>
			/// A set of <see cref="RegexOptions"/> used to control how the <see cref="Filter"/> behaves.
			/// </summary>
			public RegexOptions FilterOptions { get; }
			#endregion Properties

			#region Constructors
			/// <summary>
			/// Create a new <see cref="ConstructorArguments"/>.
			/// </summary>
			///
			/// <param name="path">
			/// The <see cref="Path"/>.
			///
			/// </param><param name="includeGitIgnore">
			/// The <see cref="IncludeGitIgnore"/>.
			///
			/// </param><param name="fileDiscoveryOptions">
			/// The <see cref="FileDiscoveryOptions"/>.
			///
			/// </param><param name="filter">
			/// The <see cref="Filter"/>.
			///
			/// </param><param name="filterOptions">
			/// The <see cref="FilterOptions"/>.
			/// </param>
			public ConstructorArguments(
				string path,
				bool includeGitIgnore,
				EnumerationOptions? fileDiscoveryOptions,
				/* TODO: Make this a Record when C# 9 releases. <2020-06-08, Iron_E> */
				(FilterType Type, string Expression)? filter = null,
				RegexOptions? filterOptions = null
			)
			{
				this.Path = path;
				this.IncludeGitIgnore = includeGitIgnore;
				this.FileDiscoveryOptions = fileDiscoveryOptions;
				this.Filter = filter;
				this.FilterOptions = filterOptions ?? RegexOptions.None;
			}
			#endregion Constructors
		} // ‡
		#endregion ConstructorArguments

		/// <summary>
		/// The pattern used to match everything with a glob.
		/// </summary>
		public const string GLOB_ALL = "*";

		#region Constructors
		/// <summary>
		/// Takes no parameters and does nothing with them.
		/// </summary>
		protected FilteredFileList()
		{
		}

		/// <summary>
		/// Create a new array of <see cref="FileSystemInfo"/> out of some <paramref name="args"/>.
		/// </summary>
		///
		/// <param name="args">
		/// The arguments for the <see cref="FilteredFileList"/>' creation.
		/// </param>
		///
		/// <returns>
		/// A <see cref="Task"/> which will yield the new <see cref="FilteredFileList"/> after it completes.
		/// </returns>
		///
		/// <exception cref="ArgumentException">
		/// <paramref name="args"/>' <see cref="ConstructorArguments.Path"/> does not exist.
		/// </exception>
		public static async Task<FilteredFileList> CreateAsync(ConstructorArguments args)
		{
			FilteredFileList @this = new FilteredFileList();

			if (File.Exists(args.Path))
			{
				@this.Capacity = 1;
				@this.Add(new FileInfo(args.Path));
			}
			else if (Directory.Exists(args.Path))
			{ await @this.AddDirectory(args).ConfigureAwait(false); }
			else { throw new ArgumentException("Invalid filesystem path."); }

			return @this;
		}
		#endregion Constructors

		#region Methods
		private async Task AddDirectory(ConstructorArguments args)
		{
			// Begin asynchronously getting the files in the current args.Path.
			Task<FileInfo[]> initialFileList = Task.Run(() =>
				// `Directory.GetFiles` returns `string`s, whereas `DirectoryInfo.GetFiles`
				//     returns `FileSystemInfo`s.
				new DirectoryInfo(args.Path).GetFiles(
					// Default to matching all files if the applied args.Filter is not a glob.
					(args.Filter?.Type == FilterType.GLOB)
						? args.Filter?.Expression
						: GLOB_ALL,
					// Provide additional options if they were passed in.
					args.FileDiscoveryOptions ?? default
				)
			);

			// if the user has not specified to include files from their gitignore
			Task<HashSet<string>>? gitLsTask = null;
			if (!args.IncludeGitIgnore && File.Exists(
				$"{args.Path}{Path.DirectorySeparatorChar}.gitignore"
			))
			{
				gitLsTask = gitListFiles();
				async Task<HashSet<string>> gitListFiles()
				{
					using ProcessReader git = GitFactory.ListFiles(args.Path);

					return (await git.ReadAllAsync().ConfigureAwait(false)).ToHashSet();
				}
			}

			// Create a regex object if the args.Filter provided specifies it.
			Regex? filterExpression = null;
			if (args.Filter.HasValue && args.Filter?.Type != FilterType.GLOB)
			{ filterExpression = new Regex(args.Filter?.Expression, args.FilterOptions); }

			Func<string, bool?>? filterFunction = null;
			if (filterExpression != null)
			{
				bool? intermediateFilterFunction(string pattern)
					=> filterExpression?.IsMatch(pattern);

				filterFunction = (args.Filter?.Type == FilterType.EXCLUDE)
					? intermediateFilterFunction
					: (Func<string, bool?>)(
						(string pattern) => !intermediateFilterFunction(pattern)
					);
			}

			if (gitLsTask != null) { _ = await gitLsTask.ConfigureAwait(false); }

			this.AddFiles(
				cwd: args.Path,
				await initialFileList.ConfigureAwait(false),
				gitLsTask?.Result,
				filterFunction
			);
		}

		/// <summary>
		/// Add <see cref="FileInfo"/>s from some <paramref name="files"/> into <c>this</c>.
		/// </summary>
		///
		/// <param name="cwd">
		/// The directory which this command was run on.
		///
		/// </param><param name="files">
		/// The new <see cref="FileInfo"/>s to add to <c>this</c>.
		///
		/// </param><param name="filesTrackedByGit">
		/// A list of <see cref="Files"/> tracked by git for comparison to <paramref name="files"/>.
		///
		/// </param><param name="filterFunction">
		/// A function used to filter out additional <see cref="FileInfo"/>s from <paramref name="files"/>.
		/// </param>
		private void AddFiles(
			string cwd,
			FileInfo[] files,
			HashSet<string>? filesTrackedByGit,
			Func<string, bool?>? filterFunction)
		{
			// Increase the capacity of this list.
			this.Capacity += files.Length;

			// Add items to this list based on the filters provided.
			for (uint i = 0; i < files.Length; ++i)
			{
				FileInfo file = files[i];
				string fileName = Path.GetRelativePath(cwd, file.FullName);

				// Assert that git is tracking all added files.
				// If there are no git files, keep going.
				if (!filesTrackedByGit?.Contains(fileName) ?? false) { continue; }

				// if ((filterFunction != null && filterFunction(fileName)) || filterFunction is null)
				if (filterFunction?.Invoke(fileName) ?? true) { this.Add(file); }
			}

			// Trim any excess capacity.
			this.TrimExcess();
		}
		#endregion Methods
	}
}
