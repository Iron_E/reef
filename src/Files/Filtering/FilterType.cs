namespace Iron_E.Reef.Files.Filtering
{
	/// <summary>
	/// The type of filter that is being applied to some file search.
	/// </summary>
	public enum FilterType
	{
		/// <summary>
		/// Like <see cref="INCLUDE"/>, but using a glob pattern.
		/// </summary>
		GLOB = 1,

		/// <summary>
		/// A filter that disallows matches of its expression from being included in a report.
		/// </summary>
		EXCLUDE = 2,

		/// <summary>
		/// An filter that only allows matches of its expression to be included in a report.
		/// </summary>
		INCLUDE = 3
	}
}
