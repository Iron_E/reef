using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

using Iron_E.Reef.IO.Async;

namespace Iron_E.Reef.Files.VCS
{
	/// <summary>
	/// Leverages the <c>git</c> command line application.
	/// </summary>
	internal static class GitFactory
	{
		#region Methods
		/// <summary>
		/// Run `git ls-files` using a <see cref="Process"/>.
		/// </summary>
		///
		/// <param name="path">
		/// The path to list files from. Defaults to the current directory.
		/// * Defaults to <see cref="Directory.GetCurrentDirectory"/>.
		/// </param>
		///
		/// <returns>
		/// * <c>Output</c>, the output of `git ls-files` from stdout.
		/// * <c>Error</c>, the output of `git ls-files` from stderr.
		/// </returns>
		///
		/// <exception cref="FileNotFoundException">
		/// * <c>git</c> failed to start.
		///
		/// </exception><exception cref="InvalidOperationException">
		/// * <c>git</c> failed to exit.
		///
		/// </exception>
		internal static ProcessReader ListFiles(string? path = null)
		{
			return new ProcessReader(new ProcessStartInfo()
			{
				Arguments = "ls-files --exclude=.gitignore",
				FileName = "git",
				WorkingDirectory = path ?? Directory.GetCurrentDirectory()
			});
		}
		#endregion Methods
	}
}
