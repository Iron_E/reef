using System;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;

namespace Iron_E.Reef.CLI
{
	[Command(
		AllowArgumentSeparator = false,
		ClusterOptions = true,
		Description = "Reef is a tool for refactoring code using regular expressions.",
		ExtendedHelpText = "\n\nAuthor:    Iron_E <code.iron.e@gmail.com>"
			+ "\nCopyright: GPLv3 © 2020"
			+ "\nRepo:      gitlab.com/Iron_E/reef"
			+ "\nVersion:   0.1.0",
		FullName = "reef",
		Name = "rf",
		ShowInHelpText = true,
		UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.Throw,
		UsePagerForHelpText = true
	)]
	public partial class Args
	{
		/// <summary>
		/// An <see cref="Exception"/> that is thrown when the user has not entered the required arguments.
		/// </summary>
		public static readonly NullReferenceException INSUFFICIENT_ARGUMENTS
			= new NullReferenceException("Required arguments have not been provided.");

		#region Methods
		/// <summary>
		/// Interpret the user's entered <see cref="Args"/> and consume them with <see cref="ArgConsumer"/>.
		/// </summary>
		///
		/// <remarks>
		/// <see cref="McMaster.Extensions.CommandLineUtils"/> detects this method by name and executes it after parsing arguments.
		/// </remarks>
		///
		/// <returns>
		/// A <see cref="Task"/> representing the state of the <c>reef</c> main process as it asynchronously executes.
		/// </returns>
		///
		/// <exception cref="NullReferenceException">
		/// When <see cref="Args"/> have not been parsed correctly.
		/// </exception>
		public async Task OnExecuteAsync()
		{
			try
			{
				ArgConsumer consumer = new ArgConsumer(this);

				await consumer.Refactor().ConfigureAwait(false);
			}
			catch (ArgumentException e) when (e.Message.StartsWith(
				"invalid pattern",
				ignoreCase: true,
				culture: null
			))
			{
				Console.Error.WriteLine(
					$"Invalid regular expression:\n\t{e.Message}"
				);
			}
		}
		#endregion Methods
	}
}
