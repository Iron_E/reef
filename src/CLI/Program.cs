﻿using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;

namespace Iron_E.Reef.CLI
{
	/// <summary>
	/// Container class for the <c>Reef</c> command line application.
	/// </summary>
	public static class Program
	{
		/// <summary>
		/// Pass <paramref name="args"/> to an argument parser and allow the <see cref="ArgConsumer"/> to perform a refactor with them.
		/// </summary>
		///
		/// <param name="args">
		/// The arguments passed to the program from the user.
		/// </param>
		///
		/// <returns>
		/// A <see cref="Task"/> which will yield a status code upon completion.
		/// </returns>
		public static Task<int> Main(string[] args)
		{
			return CommandLineApplication.ExecuteAsync<Args>(args);
		}
	}
}
