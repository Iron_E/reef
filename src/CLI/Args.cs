using McMaster.Extensions.CommandLineUtils;

namespace Iron_E.Reef.CLI
{
	/// <summary>
	/// The arguments and options that are available for <c>Reef</c>.
	/// </summary>
	public partial class Args
	{
		#region Fields & Properties
		/// <summary>
		/// The expression used to search for refactoring targets.
		/// </summary>
		[Argument(0, "search regex", "Required. The regex used to search for refactor targets.")]
		public string? RefactorSearchExpression { get; }

		/// <summary>
		/// The expression used to search for refactoring targets.
		/// </summary>
		[Argument(1, "replace regex", "Required. The regex used to replace matches of <search regex>.")]
		public string? RefactorReplaceExpression { get; }

		/// <summary>
		/// The target file or directory for this instance.
		/// </summary>
		[Argument(2, "path", "The path to look for refactor targets. Either a file or directory.")]
		[FileOrDirectoryExists]
		public string FileIncludePath { get; } = ".";
		#endregion Fields & Properties
	}
}
