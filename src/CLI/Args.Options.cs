using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using McMaster.Extensions.CommandLineUtils;

namespace Iron_E.Reef.CLI
{
	[HelpOption("-h|--help")]
	public partial class Args
	{
		#region Fields & Properties
		/// <summary>
		/// See <see cref="RegexFlags"/>.
		/// </summary>
		private RegexOptions? _regexFlags;

		/// <summary>
		/// The <see cref="RegexOptions"/> that were selected by the user.
		/// </summary>
		///
		/// <exception cref="NullReferenceException">
		/// The <see cref="_regexFlags"/> variable has somehow not been initialized.
		/// </exception>
		public RegexOptions RegexFlags
		{
			get
			{
				if (this._regexFlags is null)
				{
					RegexOptions options = RegexOptions.None;

					if (this.RegexCompile)
					{ options |= RegexOptions.Compiled; }

					if (this.RegexIgnoreCase)
					{ options |= RegexOptions.IgnoreCase; }

					if (this.RegexIgnoreWhitespace)
					{ options |= RegexOptions.IgnorePatternWhitespace; }

					this._regexFlags = options;
				}

				return this._regexFlags ?? throw new NullReferenceException(
					"RegexFlags initialization precondition has failed."
				);
			}
		}

		/// <summary>
		/// If <c>true</c>, <see cref="RegexOptions.Compiled"/> the regular expression.
		/// </summary>
		[Option(
			template: "-C|--compile",
			description: "Compile the <expression>, which makes the program take more time to start but run faster thereafter.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool RegexCompile { get; }

		/// <summary>
		/// The file extensions to include in the file system search.
		/// </summary>
		/// <example>
		/// <c>-e -cs -e csproj</c> for cscharp files.
		/// </example>
		[Option(
			template: "-e|--extension <file_extension>",
			description: "A separated list of file extensions to include in the search (all others are filtered out)."
			+ "\ne.g. '-e cs -e csproj' for csharp files.\n",
			optionType: CommandOptionType.MultipleValue
		)]
		public HashSet<string>? FileExtensions { get; }

		/// <summary>
		/// A regular expression that describes which files should be excluded.
		/// </summary>
		/// <remarks>
		/// * <see cref="FileIncludeExpression"/> takes precedence.
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-E|--exclude <regex>",
			description: "A regular expression that excludes files from the refactor. Doesn't work with -I."
			+ "\ne.g. '-E \".*bad-name.*\"'\n",
			optionType: CommandOptionType.SingleValue
		)]
		public string? FileExcludeExpression { get; }

		/// <summary>
		/// A glob used to search for files in the file system.
		/// </summary>
		/// <remarks>
		/// * Applied before <see cref="FileIncludeExpression"/>, in the initial <see cref="System.IO.DirectoryInfo.GetFiles(string, System.IO.EnumerationOptions)"/> call.
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-g|--glob <pattern>",
			description: "A glob pattern of files to include. Doesn't work with -E or -I and -g."
			+ "\ne.g. '-g *include_this_name*'\n",
			optionType: CommandOptionType.SingleValue
		)]
		public string? FileIncludeGlob { get; }

		/// <summary>
		/// If <c>true</c>, include files from <c>.gitignore</c>.
		/// </summary>
		/// <remarks>
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-G|--include-gitignore",
			description: "Search files and folders specified in a '.gitignore'. Requires `git`.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool FileIncludeGitIgnore { get; }

		/// <summary>
		/// If <c>true</c>, include hidden files and folders in the search.
		/// </summary>
		/// <remarks>
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-H|--include-hidden",
			description: "Search hidden files and folders.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool FileIncludeHidden { get; }

		/// <summary>
		/// If <c>true</c>, <see cref="RegexOptions.IgnoreCase"/>.
		/// </summary>
		/// <remarks>
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-i|--ignore-case",
			description: "Ignore differences in upper and lower case.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool RegexIgnoreCase { get; }

		/// <summary>
		/// An expression that describes which files should be included.
		/// </summary>
		/// <remarks>
		/// * Take precedence over <see cref="FileExcludeExpression"/>.
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-I|--include <regex>",
			description: "A regular expression used to include file names. Incompatible with -E and -g."
			+ "\ne.g. '-I \".*name-to-exclude.*\"'\n",
			optionType: CommandOptionType.SingleValue
		)]
		public string? FileIncludeExpression { get; }

		/// <summary>
		/// If <c>true</c>, recurse into subdirectories.
		/// * Has no effect if <see cref="FileIncludePath"/> is not a directory.
		/// </summary>
		/// <remarks>
		/// * Only works when <see cref="FileIncludePath"/> is a directory.
		/// </remarks>
		[Option(
			template: "-r|--recursive",
			description: "Recurse into subdirectories.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool FileIncludeRecursive { get; }

		/// <summary>
		/// If <c>true</c>, <see cref="RegexOptions.IgnorePatternWhitespace"/>.
		/// </summary>
		[Option(
			template: "-W|--ignore-whitespace",
			description: "Ignore whitespace when matching <expression>.\n",
			optionType: CommandOptionType.NoValue
		)]
		public bool RegexIgnoreWhitespace { get; }
		#endregion Fields & Properties
	}
}
