using System.Collections.Generic;
using System.IO;
using System;

using Iron_E.Reef.Files.Filtering;
using Iron_E.Reef.Text.DiffUtils;
using System.Threading.Tasks;

namespace Iron_E.Reef.CLI
{
	/// <summary>
	/// Represents the context to a <see cref="HunkBuilder"/> that has been given to <c>Reef</c> by a user.
	/// * Where to execute a <see cref="HunkBuilder"/>.
	/// </summary>
	internal sealed class ArgConsumer
	{
		#region Fields & Properties
		/// <summary>
		/// What this ArgConsumer's instructions are.
		/// </summary>
		internal HunkBuilder Patches { get; }

		/// <summary>
		/// Other <see cref="Args"/> that did not need to be specifically interpreted into another type.
		/// * Only refer to this if an interpreted option cannot be used instead.
		/// </summary>
		internal Args OtherOptions { get; }

		/// <summary>
		/// The target directory for this directive.
		/// </summary>
		internal List<FileInfo> FilesToRefactor { get; }
		#endregion Fields & Properties

		#region Constructors
		/// <summary>
		/// Create a new <see cref="ArgConsumer"/>.
		/// </summary>
		///
		/// <param name="args">
		/// The program <see cref="Args"/>.
		/// </param>
		///
		/// <exception cref="ArgumentException">
		/// * The specified <see cref="Args.FileIncludePath"/> does not exist.
		/// * <see cref="HunkBuilder(string, string, System.Text.RegularExpressions.RegexOptions)"/> failed.
		///
		/// </exception><exception cref="InvalidOperationException">
		/// If the <paramref name="args"/> were not parsed correctly.
		/// </exception>
		///
		/// <seealso cref="DirectoryInfo(string)">
		/// for more information about exceptions.
		///
		/// </seealso><seealso cref="FileInfo(string)">
		/// for more information about exceptions.
		/// </seealso>
		internal ArgConsumer(Args args)
		{
			// Determine the type of filter being used ahead of time, since it will be
			//     referenced more than once.
			FilterType? filterType = args switch
			{
				var a when a.FileIncludeGlob != null => FilterType.GLOB,
				var a when a.FileIncludeExpression != null => FilterType.INCLUDE,
				var a when a.FileExcludeExpression != null => FilterType.EXCLUDE,
				_ => null
			};

			// Create the file discovery options out of the `Args` from the user.
			EnumerationOptions fileDiscoveryOptions = new EnumerationOptions()
			{
				MatchCasing = args.RegexIgnoreCase
					? MatchCasing.CaseInsensitive
					: MatchCasing.CaseSensitive,
				RecurseSubdirectories = args.FileIncludeRecursive,
				AttributesToSkip = !args.FileIncludeHidden
					? FileAttributes.Hidden
					: default
			};

			// Define an asynchronous method to create a field for the class.
			async Task<FilteredFileList> createRefactorTargets(
				FilterType? type = null, string? expression = null
			)
			{
				return (expression != null && type.HasValue)
					? await FilteredFileList.CreateAsync(
						new FilteredFileList.ConstructorArguments(
							args.FileIncludePath,
							args.FileIncludeGitIgnore,
							fileDiscoveryOptions,
							(type.Value, expression),
							args.RegexFlags
						)
					).ConfigureAwait(false)
					: await FilteredFileList.CreateAsync(
						new FilteredFileList.ConstructorArguments(
							args.FileIncludePath,
							args.FileIncludeGitIgnore,
							fileDiscoveryOptions
						)
					).ConfigureAwait(false);
			}

			// Begin doing work on the other thread to create the refactor target.
			Task<FilteredFileList> filesToRefactor = filterType switch
			{
				FilterType.GLOB => createRefactorTargets(
					FilterType.GLOB, args.FileIncludeGlob
				),
				FilterType.EXCLUDE => createRefactorTargets(
					FilterType.EXCLUDE, args.FileExcludeExpression
				),
				FilterType.INCLUDE => createRefactorTargets(
					FilterType.INCLUDE, args.FileIncludeExpression
				),
				_ => createRefactorTargets()
			};

			NullReferenceException invalidArguments = new NullReferenceException(
				"Required arguments have not been provided."
			);

			// While `filesToRefactor` is still working, create a `PatchProvider`.
			this.Patches = new HunkBuilder(
				args.RefactorSearchExpression ?? throw invalidArguments,
				args.RefactorReplaceExpression ?? throw invalidArguments,
				args.RegexFlags
			);

			// Assign the options as well.
			this.OtherOptions = args;

			// Finally, wait for the task to finish and assign the result.
			this.FilesToRefactor = filesToRefactor.Result;
		}
		#endregion Constructors

		#region Methods
		/// <summary>
		/// Perform a refactor on <see cref="FilesToRefactor"/>.
		/// </summary>
		///
		/// <returns>
		/// A <see cref="Task"/> containing the state of this method while it executes asynchronously.
		/// </returns>
		internal async Task Refactor()
		{
			this.FilesToRefactor.ForEach(Console.WriteLine);
			// PLACEHOLDER CODE:
			await Task.Yield();
		}
		#endregion Methods

	}
}
