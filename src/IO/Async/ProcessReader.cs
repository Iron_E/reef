using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Iron_E.Reef.IO.Async
{
	/// <summary>
	/// Leverages the <see cref="WrappedProcess"/> command line application.
	/// </summary>
	public class ProcessReader: IDisposable, IAsyncDisposable
	{
		#region Fields & Properties
		/// <summary>
		/// The <see cref="Exception"/> to throw when <see cref="WrappedProcess"/> doesn't start.
		/// </summary>
		private static readonly FileNotFoundException START_FAILED
			= new FileNotFoundException("Could not start the `WrappedProcess`.");

		/// <summary>
		/// If true, <c>this</c> has been <see cref="Dispose(bool)"/>ed.
		/// If false, it has not.
		/// </summary>
		private bool _disposed { get; set; }

		/// <summary>
		/// The process that <c>this</c> is reading from.
		/// </summary>
		public Process WrappedProcess { get; }
		#endregion Fields & Properties

		#region Constructors
		/// <summary>
		/// Create a new <see cref="ProcessReader"/>.
		/// </summary>
		/// <param name="configuration">
		/// The <see cref="ProcessStartInfo"/> to use for <see cref="WrappedProcess"/>.
		/// </param>
		public ProcessReader(ProcessStartInfo configuration)
		{
			this.WrappedProcess = new Process() { StartInfo = configuration };
		}

		/// <summary>
		/// <para>
		/// Destructor for <see cref="ProcessReader"/>.
		/// </para>
		///
		/// <para>
		/// Calls <see cref="Dispose()"/> in case the user forgot to.
		/// </para>
		/// </summary>
		~ProcessReader() { this.Dispose(); }
		#endregion Constructors

		#region IDisposable
		/// <summary>
		/// Dispose of internally held resources.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>
		/// <see cref="Dispose()"/> asynchronously.
		/// </summary>
		///
		/// <returns>
		/// The <see cref="ValueTask"/> associated with <see cref="Dispose()"/>.
		/// </returns>
		public ValueTask DisposeAsync()
		{
			return new ValueTask(Task.Run(this.Dispose));
		}

		/// <summary>
		/// Initiate internal disposal of currently owned resources.
		/// </summary>
		///
		/// <param name="disposing">
		/// Whether or not to recursively dispose of properties.
		/// </param>
		protected virtual void Dispose(bool disposing)
		{
			if (!this._disposed)
			{
				// Dispose embedded resources.
				if (disposing) { this.WrappedProcess.Dispose(); }

				// Make sure this method is not run again.
				this._disposed = true;
			}
		}
		#endregion IDisposable

		#region Methods
		/// <summary>
		/// Run `git ls-files` using a <see cref="Process"/>.
		/// </summary>
		///
		/// <remarks>
		/// <para>
		/// One can use <see cref="Task.WhenAny{TResult}(Task{TResult}[])"/> and <see cref="Task.Delay(int)"/> to add a timeout.
		/// </para>
		///
		/// <code>
		/// var readTask = processReader.ReadToEndAsync();
		/// await Task.WhenAny(readTask, Task.Delay(10000));
		///
		/// if (!readTask.IsCompletedSuccessfully) { /* handle errors. */ }
		/// </code>
		/// </remarks>
		///
		/// <returns>
		/// The <see cref="Process.StandardOutput"/> split into lines with <see cref="Environment.NewLine"/>.
		/// </returns>
		///
		/// <exception cref="FileNotFoundException">
		/// The <see cref="WrappedProcess"/> specified <see cref="ProcessStartInfo.FileName"/> does not exist.
		///
		/// </exception><exception cref="InvalidOperationException">
		/// The <see cref="WrappedProcess"/> exited with an error code.
		///
		/// </exception><exception cref="IOException">
		/// The <see cref="Process.StandardOutput"/>'s <see cref="StreamReader.ReadToEndAsync()"/>'s <see cref="Task.IsCompletedSuccessfully"/> property is <c>false</c>.
		///
		/// </exception><exception cref="EndOfStreamException">
		/// The <see cref="WrappedProcess"/> exits without writing to <see cref="Process.StandardOutput"/>.
		/// </exception>
		public async Task<string[]> ReadAllAsync()
		{
			// Setup the process start info
			this.WrappedProcess.StartInfo.RedirectStandardError = true;
			this.WrappedProcess.StartInfo.RedirectStandardOutput = true;
			this.WrappedProcess.StartInfo.UseShellExecute = false;

			// Start the process and begin reading from stdout.
			if (!this.WrappedProcess.Start()) { throw START_FAILED; }

			Task<string> stderrTask = this.WrappedProcess.StandardError.ReadToEndAsync();
			Task<string> stdoutTask = this.WrappedProcess.StandardOutput.ReadToEndAsync();

			await Task.WhenAll(stdoutTask, stderrTask, Task.Run(
				this.WrappedProcess.WaitForExit
			)).ConfigureAwait(false);

			/* TODO: Fix this exit code logic. <2020-06-11, Iron_E> */
			if (this.WrappedProcess.ExitCode != 0)
			{
				throw new InvalidOperationException(
					$"Sub-process '{this.WrappedProcess.StartInfo.FileName}' exited with error code {this.WrappedProcess.ExitCode}."
					+ $" See below:{Environment.NewLine}{stderrTask.Result}"
				);
			}
			else if (!stdoutTask.IsCompletedSuccessfully)
			{
				throw new IOException(
					$"Reading stdout from '{this.WrappedProcess.StartInfo.FileName}' failed."
					+ $" See below:{Environment.NewLine}{stderrTask.Result}"
				);
			}
			else if (string.IsNullOrEmpty(stdoutTask.Result))
			{
				throw new EndOfStreamException(
					$"Sub-process '{this.WrappedProcess.StartInfo.FileName}' completed without writing to stdout."
					+ $" See below:{Environment.NewLine}{stderrTask.Result}"
				);
			}

			return stdoutTask.Result.Split(Environment.NewLine);
		}
		#endregion Methods
	}
}
