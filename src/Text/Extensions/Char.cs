using System;
using System.Collections.Generic;
using System.Linq;

namespace Iron_E.Reef.Text.Extensions
{
	/// <summary>
	/// A class with extensions for the <see cref="char"/> class.
	/// </summary>
	public static class Char
	{
		/// <summary>
		/// Determine whether or not <paramref name="this"/> is a newline or not.
		/// </summary>
		///
		/// <param name="this">
		/// The <see cref="char"/> to test.
		/// </param>
		///
		/// <returns>
		/// Whether or
		/// </returns>
		public static bool IsNewLine(this char @this) => @this == '\n';

	}
}
