using System;
using System.Collections.Generic;
using System.Linq;

namespace Iron_E.Reef.Text.Extensions
{
	/// <summary>
	/// A class with extensions for the <see cref="string"/> class.
	/// </summary>
	public static class String
	{
		#region String.SpanEnumerator
		private sealed class SpanEnumerator : IEqualityComparer<ReadOnlySpan<char>>
		{
			public static readonly SpanEnumerator Instance = new SpanEnumerator();

			private SpanEnumerator() { }

			/// <inheritdoc cref="IEqualityComparer{ReadOnlySpan}.Equals(ReadOnlySpan, ReadOnlySpan)"/>
			public bool Equals(ReadOnlySpan<char> x, ReadOnlySpan<char> y)
			{
				if (x.Length != y.Length) { return false; }

				for (int i = 0; i < x.Length; ++i)
				{ if (x[i] != y[i]) { return false; } }

				return true;
			}

			public int GetHashCode(ReadOnlySpan<char> obj)
			{
				int code = 0;

				foreach (char c in obj)
				{ code = HashCode.Combine(code, c); }

				return code;
			}
		}
		#endregion String.SpanEnumerator

		/// <summary>
		/// Count the number of <c>\n</c> <see cref="char"/>acters there are in <c>this</c> <see cref="string"/>.
		/// </summary>
		///
		/// <param name="this">
		/// The <see cref="string"/> to count the <see cref="Environment.NewLine"/>'s of.
		/// </param>
		///
		/// <returns>
		/// The number of <c>\n</c> <see cref="char"/>acters in <c>this</c> <see cref="string"/>.
		/// </returns>
		public static uint CountNewLines(this string @this)
		{
			ReadOnlySpan<char> span = @this.AsSpan();
			uint newLines = 0;

			// Made this value a const for clarity.
			const int CURRENT_CHAR = 1;
			for (int offset = 0; offset < span.Length; ++offset)
			{
				int endIndex = offset - CURRENT_CHAR + Environment.NewLine.Length;

				if (endIndex > span.Length) { break; }

				if (SpanEnumerator.Instance.Equals(
					Environment.NewLine.AsSpan(), span([offset..endIndex])
				) { ++newLines; }
			}

			return newLines;
		}
	}
}
