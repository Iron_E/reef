namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// Methods that define how some structure may be patched.
	/// </summary>
	public interface IPatchable<T>
	{
		#region Properties
		/// <summary>
		/// The type used to patch.
		/// </summary>
		T Target { get; }
		#endregion Properties

		#region Methods
		/// <summary>
		/// Apply an <see cref="IHunk"/> to <see cref="Target"/>.
		/// </summary>
		void Patch(IHunk hunk);
		/// <summary>
		/// Apply a <see cref="IDiff"/> to <see cref="Target"/>.
		/// </summary>
		void Patch(IDiff diff)
		{
			foreach (IHunk hunk in diff.Hunks.Values)
			{ this.Patch(hunk); }
		}
		#endregion Methods
	}
}
