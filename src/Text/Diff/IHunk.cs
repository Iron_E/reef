using System;
using System.Linq;

using static Iron_E.Reef.Text.Extensions.Char;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// Represents a result of a <see cref="System.Text.RegularExpressions"/> match which is being presented to the user for approval.
	/// </summary>
	public interface IHunk : IComparable<IHunk>, IEquatable<IHunk>
	{
		#region IHunk.Header
		/// <summary>
		/// Represents part of an <see cref="IHunk"/>'s <see cref="Header"/>.
		/// </summary>
		/* TODO: Make this a Record when C# 9 releases. <2020-06-11, Iron_E>*/
		public readonly struct Header
		{
			#region Properties
			/// <summary>
			/// The string representation of <c>this</c> <see cref="Header"/>.
			/// </summary>
			public string AsString { get; }

			/// <summary>
			/// The cursor position which this part of the <see cref="Header"/> starts on (inclusive).
			/// </summary>
			public uint IndexBegin { get; }

			/// <summary>
			/// The ending index of an <see cref="IHunk"/> after it is applied.
			/// </summary>
			public uint IndexEndAddition { get; }

			/// <summary>
			/// The cursor position which this part of the <see cref="Header"/> ends on (exclusive).
			/// </summary>
			public uint IndexEndDeletion { get; }

			/// <summary>
			/// The line number which this part of the <see cref="Header"/> starts on.
			/// </summary>
			public uint LineNumberBegin { get; }

			/// <summary>
			/// Get the line that the <see cref="IndexEndAddition"/> resides on.
			/// </summary>
			public uint LineNumberEndAddition { get; }

			/// <summary>
			/// Get the line that the <see cref="IndexEndDeletion"/> resides on.
			/// </summary>
			public uint LineNumberEndDeletion { get; }
			#endregion Properties

			#region Constructors
			/// <summary>
			/// Create a new <see cref="Header"/>.
			/// </summary>
			///
			/// <param name="startLineNumber">
			/// The <see cref="LineNumberBegin"/>.
			///
			/// </param><param name="startIndex">
			/// The <see cref="IndexBegin"/>
			///
			/// </param><param name="deletion">
			/// The string of text that has been removed from the outer <see cref="IHunk"/>.
			///
			/// </param><param name="addition">
			/// The string of text that has been added in place of <paramref name="deletion"/> to the outer <see cref="IHunk"/>.
			/// </param>
			public Header(uint startLineNumber, uint startIndex, string deletion, string addition)
			{
				this.IndexBegin = startIndex;
				this.IndexEndDeletion = startIndex + (uint)deletion.Length;
				this.IndexEndAddition = startIndex + (uint)addition.Length;

				this.LineNumberBegin = startLineNumber;
				this.LineNumberEndDeletion = startLineNumber + (uint)deletion.Count((char c) => c.IsNewLine());
				this.LineNumberEndAddition = startLineNumber + (uint)addition.Count((char c) => c.IsNewLine());

				this.AsString = $"@@ -{startLineNumber},{this.LineNumberEndDeletion - startLineNumber} +{startLineNumber},{this.LineNumberEndAddition - startLineNumber} @@";
			}
			#endregion Constructors
		}
		#endregion IHunk.Header

		#region Fields & Properties
		/// <summary>
		/// The <see cref="string"/> that has been suggested to replace some line.
		/// </summary>
		string Addition { get; }

		/// <summary>
		/// The match that this patch is for.
		/// </summary>
		string Deletion { get; }

		/// <summary>
		/// The range in some file where the <see cref="Addition"/> should be inserted.
		/// </summary>
		Header Range { get; }
		#endregion Fields & Properties

		#region Implement IComparable
		/// <inheritdoc cref="IComparable{IHunk}.CompareTo(IHunk)"/>.
		int IComparable<IHunk>.CompareTo(IHunk other)
		{
			int comparisonResult = this.Range.LineNumberBegin.CompareTo(other.Range.LineNumberBegin);

			bool comparisonWasEqual() => comparisonResult == 0;

			if (comparisonWasEqual())
			{
				comparisonResult = this.Range.IndexBegin.CompareTo(other.Range.IndexBegin);

				if (comparisonWasEqual())
				{
					comparisonResult = this.Range.IndexEndDeletion.CompareTo(other.Range.IndexEndDeletion);

					if (comparisonWasEqual())
					{ comparisonResult = this.Range.IndexEndAddition.CompareTo(other.Range.IndexEndDeletion); }
				}
			}

			return comparisonResult;
		}
		#endregion Implement IComparable

		#region Implement IEquatable
		/// <inheritdoc cref="IEquatable{IHunk}.Equals(IHunk)"/>.
		bool IEquatable<IHunk>.Equals(IHunk other) => this.CompareTo(other) == 0
			&& this.Addition.Equals(other.Addition)
			&& this.Deletion.Equals(other.Deletion);

		/// <inheritdoc cref="object.Equals(object)"/>
		bool Equals(object? obj) => !(obj is null) && obj is Hunk hunk
			&& ((IEquatable<IHunk>)this).Equals(hunk);

		/// <inheritdoc cref="object.GetHashCode"/>
		int GetHashCode() => HashCode.Combine(this.Addition, this.Deletion, this.Range);
		#endregion Implement IEquatable
	}
}
