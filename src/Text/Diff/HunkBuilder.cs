using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// Provides <see cref="Hunk"/>es for text given some <see cref="SearchPattern"/> and <see cref="ReplacePattern"/>.
	/// </summary>
	public class HunkBuilder
	{
		#region Fields & Properties
		/// <summary>
		/// The regular expression used to find matches.
		/// </summary>
		/// <remarks>
		/// The .NET Regex parser is very mature and is capable of many advanced features.
		/// * See here: https://docs.microsoft.com/en-us/dotnet/standard/base-types/substitutions-in-regular-expressions
		/// </remarks>
		public Regex SearchPattern { get; }
		/// <summary>
		/// The regular expression used to replace matches.
		/// </summary>
		/// <remarks>
		/// The .NET Regex parser is very mature and is capable of many advanced features.
		/// * See here: https://docs.microsoft.com/en-us/dotnet/standard/base-types/substitutions-in-regular-expressions
		/// </remarks>
		public string ReplacePattern { get; }
		#endregion Fields & Properties
		#region Constructors
		/// <summary>
		/// Create a new <see cref="HunkBuilder"/>
		/// </summary>
		///
		/// <param name="search">
		/// The <see cref="SearchPattern"/>.
		/// </param>
		///
		/// <param name="replace">
		/// The <see cref="ReplacePattern"/>.
		/// </param>
		///
		/// <param name="options">
		/// The <see cref="RegexOptions"/> to use when running.
		/// </param>
		///
		/// <exception cref="System.ArgumentException">
		/// If <paramref name="search"/> is not a valid <see cref="Regex"/>.
		/// </exception>
		public HunkBuilder(string search, string replace, RegexOptions options = RegexOptions.None)
		{
			this.SearchPattern = new Regex(search, options);

			this.ReplacePattern = replace;
		}
		#endregion Constructors
		#region Methods
		/// <summary>
		/// Create an <see cref="IEnumerable{IHunk}"/> from some <paramref name="text"/>.
		/// </summary>
		///
		/// <param name="text">
		/// The text to generate <see cref="IHunk"/>s for.
		/// </param>
		///
		/// <returns>
		/// The <see cref="IHunk"/>s created from <paramref name="text"/>.
		/// </returns>
		public IEnumerable<IHunk> Create(string text)
			=> this.SearchPattern.Matches(text).Select(
				(Match match) => new Hunk(
					startLineNumber: (uint)text.AsSpan()[..match.Index].LastIndexOf('\n'),
					startIndex: (uint)match.Index,
					deletion: match.Result(this.ReplacePattern),
					addition: match.Value
				)
			);
		#endregion Methods
	}
}
