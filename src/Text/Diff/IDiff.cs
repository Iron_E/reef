using System;
using System.Collections.Generic;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// The sum of multiple <see cref="IHunk"/>s.
	/// </summary>
	public interface IDiff
	{
		#region Properties
		/// <summary>
		/// The hunks that are a part of this <see cref="IDiff"/>.
		/// </summary>
		/// <remarks>
		/// The <see cref="IHunk"/>s are meant to be sorted by <see cref="IHunk.Header.IndexBegin"/>
		/// </remarks>
		public SortedList<uint, IHunk> Hunks { get; }
		#endregion Properties

		#region Methods
		/// <summary>
		/// Format <c>this</c> <see cref="IDiff"/> into text.
		/// </summary>
		///
		/// <param name="formatter">
		/// What <see cref="IDiffFormatter"/> the <see cref="IDiff"/>
		/// </param>
		public string ToString(IDiffFormatter formatter)
			=> formatter.Format(this);
		#endregion Methods
	}
}
