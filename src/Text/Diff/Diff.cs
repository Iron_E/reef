using System;
using System.Collections.Generic;
using System.Linq;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <inheritdoc cref="IDiff"/>
	public sealed class Diff : IDiff
	{
		#region Properties
		/// <inheritdoc cref="IDiff.Hunks"/>
		public SortedList<uint, IHunk> Hunks { get; }
		#endregion Properties

		#region Constructors
		/// <summary>
		/// The default constructor. Creates a new <see cref="Hunks"/> list.
		/// </summary>
		public Diff() { this.Hunks = new SortedList<uint, IHunk>(); }

		/// <summary>
		/// The default constructor. Creates a new <see cref="Hunks"/> list out of another list of <paramref name="hunks"/>.
		/// </summary>
		///
		/// <param name="hunks">
		/// The hunks to sort and add to <see cref="Hunks"/>.
		/// </param>
		public Diff(IEnumerable<IHunk> hunks)
		{
			this.Hunks = new SortedList<uint, IHunk>(
				new Dictionary<uint, IHunk>(hunks.Select(
					(IHunk hunk) => KeyValuePair.Create(hunk.Range.IndexBegin, hunk)
				))
			);
		}
		#endregion Constructors
	}
}
