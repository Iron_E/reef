using System.Text.RegularExpressions;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <inheritdoc cref="IHunk"/>
	/* TODO: Make this class a Record when C# 9 releases. <2020-06-12, Iron_E> */
	internal sealed class Hunk : IHunk
	{
		#region Properties
		/// <inheritdoc cref="IHunk.Addition"/>
		public string Addition { get; }
		/// <inheritdoc cref="IHunk.Range"/>
		public IHunk.Header Range { get; }
		/// <inheritdoc cref="IHunk.Deletion"/>
		public string Deletion { get; }
		#endregion Properties
		#region Constructors
		/// <summary>
		/// Create a new <see cref="Hunk"/>.
		/// </summary>
		///
		/// <param name="startLineNumber">
		/// The <see cref="IHunk.Header.LineNumberBegin"/>.
		///
		/// </param><param name="startIndex">
		/// The <see cref="IHunk.Header.IndexBegin"/>.
		///
		/// </param><param name="deletion">
		/// The <see cref="Addition"/>.
		///
		/// </param><param name="addition">
		/// The <paramref name="deletion"/>'s <see cref="Match.Result(string)"/>
		///
		/// </param>
		internal Hunk(uint startLineNumber, uint startIndex, string deletion, string addition)
		{
			this.Addition = addition;
			this.Deletion = deletion;
			this.Range = new IHunk.Header(startLineNumber, startIndex, deletion, addition);
		}
		#endregion Constructors
	}
}
