using System;
using System.Collections.Generic;
using System.Linq;

using Text = System.Text;

using Iron_E.Reef.Text.DiffUtils;

namespace Iron_E.Reef.Text.DiffUtils.Extensions
{
	/// <summary>
	/// Extension methods for the <see cref="Text::StringBuilder"/> class that improve its ability to interact with <see cref="IHunk"/>es.
	/// </summary>
	public static class StringBuilder
	{
		/// <summary>
		/// <paramref name="prefix"/> a region of <c>this</c> <see cref="string"/> for an <see cref="IDiffFormatter"/>.
		/// </summary>
		///
		/// <param name="this">
		/// The <see cref="Text::StringBuilder"/> to perform this operation on.
		/// </param>
		///
		/// <param name="toAppend">
		/// The string to append to <c>this</c>.
		///
		/// </param><param name="prefix">
		/// What <see cref="char"/> to prefix lines with.
		/// </param>
		///
		/// <returns>
		/// <c>this</c> <see cref="Text::StringBuilder"/> after <see cref="AppendWithPrefix"/> has been applied.
		/// </returns>
		public static Text::StringBuilder AppendWithPrefix(
			this Text::StringBuilder @this,
			string toAppend, char prefix = ' '
		)
		{
			int startIndex = @this.Length;

			_ = @this.EnsureCapacity(@this.Length + toAppend.Length);
			_ = @this.Append(toAppend);

			int endIndex = @this.Length;

			// The initial capcity is the worst-case scenario.
			List<int> newLineIndexes = new List<int>(
				capacity: endIndex - startIndex
			);

			// StringBuilder doesn't have any fancy searching methods,
			//    so we have to do this manually.
			for (int i = startIndex; i < endIndex; ++i)
			{ if (@this[i].IsNewLine()) { newLineIndexes.Add(i); } }

			// If there is extra space being used, we don't need it.
			//     There won't be any more writing to the list.
			newLineIndexes.TrimExcess();

			// We will, however, need to increase the capacity of this
			//     array.
			@this.Capacity += newLineIndexes.Count;

			// This is where we put all the prefixes in.
			foreach (int index in newLineIndexes)
			{ _ = @this.Insert(index, prefix); }

			return @this;
		}
	}
}
