using Text = System.Text;

namespace Iron_E.Reef.Text.DiffUtils
{
	public sealed class Patchable : IPatchable<Text::StringBuilder>
	{
		#region Properties
		/// <summary>
		/// The type used to patch.
		/// </summary>
		Text::StringBuilder Target { get; }
		#endregion Properties

		#region Constructors
		/// <summary>
		/// Create a <see cref="Patchable"/> using <see cref="Text::StringBuilder()"/>
		/// </summary>
		public Patchable() : this(new Text::StringBuilder())
		{
		}

		/// <summary>
		/// Create a <see cref="Patchable"/> from some pre-existing <paramref name="text"/>.
		/// </summary>
		///
		/// <param name="text">
		/// The text to use for <c>this</c>.
		/// </param>
		public Patchable(Text::StringBuilder text) { this.Target = text; }
		#endregion Constructors

		#region Methods
		/// <inheritdoc cref="IPatchable{StringBuilder}.Patch(IHunk)"/>
		public void Patch(IHunk hunk)
		{
			_ = this.Target.Remove(
				(int)hunk.Range.IndexBegin, hunk.Deletion.Length
			).Insert(
				(int)hunk.Range.IndexBegin, hunk.Addition
			);
		}
		#endregion Methods
	}
}
