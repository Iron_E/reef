using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using static Iron_E.Reef.Text.Extensions.Char;
using static Iron_E.Reef.Text.Extensions.String;
using static Iron_E.Reef.Text.DiffUtils.Extensions.StringBuilder;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// Responsible for creating a unified diff out of an <see cref="OriginalText"/> and some <see cref="IHunk"/>s.
	/// </summary>
	public class UnifiedDiffFormatter : IDiffFormatter
	{
		#region Properties
		/// <inheritdoc cref="IDiffFormatter{T}.LinesOfContext"/>
		public ushort LinesOfContext { get; }
		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.OriginalText"/>
		public ReadOnlyMemory<char> OriginalText { get; }
		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.OriginalTextLines"/>
		public uint OriginalTextLines { get; }
		#endregion Properties

		/* TODO: Implement more constructors. <2020-06-11, Iron_E> */
		#region Constructors
		/// <summary>
		/// Create a <c>DiffDeriver</c> from some <paramref name="originalText"/>.
		/// </summary>
		///
		/// <param name="originalText">
		/// The <see cref="OriginalText"/>.
		///
		/// </param><param name="linesOfContext">
		/// The <see cref="LinesOfContext"/>.
		/// </param>
		public UnifiedDiffFormatter(string originalText, ushort linesOfContext = 3)
		{
			this.LinesOfContext = linesOfContext;
			this.OriginalText = originalText.AsMemory();
			this.OriginalTextLines = originalText.CountNewLines();
		}
		#endregion Constructors
		#region Methods
		/// <inheritdoc cref="IDiffFormatter.Append(StringBuilder, IHunk)"/>
		public StringBuilder Append(StringBuilder diffInProgress, IHunk hunk)
		{
			var beforeHunk = this.ContextBefore(hunk);
			var afterHunk = this.ContextAfter(hunk);

			const int HUNK_RANGE_LINES = 1;

			int diffLength = beforeHunk.FirstLines.Length
				+ (// hunk length
					HUNK_RANGE_LINES
					+ hunk.Range.AsString.Length
					+ (2 * beforeHunk.SameLine.Length)
					+ hunk.Deletion.Length
					+ hunk.Addition.Length
					+ (2 * afterHunk.SameLine.Length)
				)
				+ afterHunk.LastLines.Length;

			_ = diffInProgress.EnsureCapacity(diffInProgress.Length + diffLength);

			return diffInProgress.AppendWithPrefix(
				beforeHunk.FirstLines
			).AppendWithPrefix(
				$"{beforeHunk.SameLine}{hunk.Deletion}{afterHunk.SameLine}",
				prefix: '-'
			).AppendWithPrefix(
				$"{beforeHunk.SameLine}{hunk.Addition}{afterHunk.SameLine}",
				prefix: '+'
			).AppendWithPrefix(afterHunk.LastLines);
		}
		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.ContextAfter(IHunk)"/>
		public (ReadOnlyMemory<char> SameLine, string LastLines) ContextAfter(IHunk hunk)
		{
			// Get the total block of text AFTER the diff.
			ReadOnlyMemory<char> afterHunk = this.OriginalText[(int)hunk.Range.IndexEndDeletion..];

			// Create variables that will hold information about the
			var context = (
				(IDiffFormatter<ReadOnlyMemory<char>>)this
			).ContextInfoAfter(afterHunk.Span);

			return (
				SameLine: context.IndexLineBegin > 0
					? afterHunk[..context.IndexLineBegin]
					: afterHunk,
				LastLines: (context.Lines == 0 || this.LinesOfContext == 0)
					? string.Empty
					: afterHunk[context.IndexLineBegin..context.IndexLineEnd].ToString()
			);
		}
		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.ContextBefore(IHunk)"/>
		public (string FirstLines, ReadOnlyMemory<char> SameLine) ContextBefore(IHunk hunk)
		{
			// Get the total block of text BEFORE the diff.
			ReadOnlyMemory<char> beforeHunk = this.OriginalText[..(int)hunk.Range.IndexBegin];

			// Find the final new line.
			var context = (
				(IDiffFormatter<ReadOnlyMemory<char>>)this
			).ContextInfoBefore(beforeHunk.Span);

			return (
				FirstLines: (context.Lines == 0 || this.LinesOfContext == 0)
					? string.Empty
					: beforeHunk[context.IndexLineBegin..context.IndexLineEnd].ToString(),
				SameLine: context.IndexLineBegin > 0
					? beforeHunk[context.IndexLineBegin..]
					: beforeHunk
			);
		}

		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.Format(IDiff)"/>
		/* TODO: Finish this. <2020-06-12, Iron_E> */
		public string Format(IDiff diff)
		{
			StringBuilder diffBuilder = new StringBuilder((int)(
				this.OriginalText.Length + this.OriginalTextLines
			)).Append(this.OriginalText);

			for (int index = 0; index < diff.Hunks.Count; ++index)
			{
				IHunk current = diff.Hunks.ElementAt(index).Value;

				_ = this.Append(diffBuilder, current);

				for (
					// Get the element at the next index.
					IHunk next = diff.Hunks.ElementAtOrDefault(++index);

					// Execute only if two conditions are met:
					//
					// 1. The next `IHunk` exists (aka is not `default`).
					// 2. The ending context line of the current IHunk is
					//    overlapping the beginning line of the next IHunk.
					next != default && current.Range.LineNumberBegin
						+ this.LinesOfContext >= next.Range.LineNumberBegin;
				)
				{
					// Insert the next hunk into the current hunk.
					_ = this.Include(diffBuilder, next,
						new ReadOnlySpan<char>(diff.Hunks.Values, 0, index)
					);
				}
			}

			return diffBuilder.ToString();
		}

		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.Format(IHunk)"/>
		/* TODO: Test this method. <2020-06-12, Iron_E> */
		public string Format(IHunk hunk) => this.Append(new StringBuilder(), hunk).ToString();

		/// <inheritdoc cref="IDiffFormatter{ReadOnlyMemory}.Include(StringBuilder, IHunk, ReadOnlySpan{IHunk})"/>
		/* TODO: Test this method. <2020-06-15, Iron_E> */
		public StringBuilder Include(StringBuilder diffInProgress, IHunk newHunk, ReadOnlySpan<IHunk> includedHunks)
		{
			// If the `hunkIndex` is intended to be used, this value will be greater than zero.
			int insertIndex = ~includedHunks.BinarySearch(newHunk);

			// the `hunk` already has been included.
			if (insertIndex < 0) { return diffInProgress; }
			else if (insertIndex == 0) // there is no parent hunk; it must be inserted.
			{
				return diffInProgress.Insert(
					0, this.Append(new StringBuilder(), newHunk)
				);
			}
			// We have to decrement the index to get the parent.
			else { --insertIndex; }

			// The original hunk that was located at the insertion index.
			IHunk originalHunk = includedHunks[insertIndex];

			// Get the position of the
			static ushort length() => originalHunk.

			int originalHunkStartIndex = includedHunks.;
			int originalHunkEndIndex;

			// TODO: create logic to determine which one is the parent.

			if (parent.Equals(this))
			{

			}
			else
			{

			}

			/* ==================================================================================================================== */
			if (newHunk.Range.IndexBegin < parent.Range.IndexBegin || newHunk.Range.LineNumberBegin < parent.Range.LineNumberBegin)
			{ throw new ArgumentOutOfRangeException("`hunk` cannot be put `parent` an `IHunk` that it comes before."); }

			/* TODO:
			 * - Get `indexOffset`, since the `precedingHunks` and their headers have caused an offset to occur in number of characters.
			 * - In order to write parent the file, we need to know where we are.
			 * <2020-06-13, Iron_E> */
			return diffInProgress.Remove(
				(int)newHunk.Range.IndexBegin,
				(int)(newHunk.Range.IndexEndDeletion - newHunk.Range.IndexBegin)
			).Insert(
				newHunk.Range.IndexBegin,
				newHunk.Addition
			);
		}
		#endregion Methods
	}
}
