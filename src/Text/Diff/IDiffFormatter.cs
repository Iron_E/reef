using System;
using System.Collections.Generic;
using System.Text;

using static Iron_E.Reef.Text.Extensions.Char;
using static Iron_E.Reef.Text.Extensions.String;

namespace Iron_E.Reef.Text.DiffUtils
{
	/// <summary>
	/// The
	/// </summary>
	public interface IDiffFormatter
	{
		#region Properties
		/// <summary>
		/// How many lines of context to provide on both sides of the diff.
		/// </summary>
		ushort LinesOfContext { get; }
		/// <summary>
		/// A list of the lines that <c>this</c> should use to calculate diffs.
		/// </summary>
		ReadOnlyMemory<char> OriginalText { get; }
		/// <summary>
		/// The number of <c>\n</c> characters in <see cref="OriginalText"/>.
		/// </summary>
		uint OriginalTextLines { get; }
		#endregion Properties
		#region Methods
		/// <summary>
		/// Append some <paramref name="hunk"/> to the end of <paramref name="diffInProgress"/>.
		/// </summary>
		///
		/// <param name="diffInProgress">
		/// The string diff being built.
		///
		/// </param><param name="hunk">
		/// The hunk to append to the end of <paramref name="diffInProgress"/>.
		/// </param>
		///
		/// <returns>
		/// <paramref name="diffInProgress"/> after <paramref name="hunk"/> is appended.
		/// </returns>
		StringBuilder Append(StringBuilder diffInProgress, IHunk hunk);
		/// <summary>
		/// Get the portion after some <paramref name="hunk"/>.
		/// </summary>
		///
		/// <param name="hunk">
		/// A diff hunk in reference to the original text of this formatter.
		/// </param>
		///
		/// <returns>
		/// The <c>LastLines</c> of the original text, and the part that is on the <c>SameLine</c> as the <see cref="IHunk.Deletion"/>.
		/// </returns>
		(ReadOnlyMemory<char> SameLine, string LastLines) ContextAfter(IHunk hunk);
		/// <summary>
		/// Get the portion before some <paramref name="hunk"/>.
		/// </summary>
		///
		/// <param name="hunk">
		/// A diff <see cref="IHunk"/> in reference to the original text.
		/// </param>
		///
		/// <returns>
		/// The <c>FirstLines</c> of the original text, and the part that is on the <c>SameLine</c> as the <see cref="IHunk.Deletion"/>.
		/// </returns>
		(string FirstLines, ReadOnlyMemory<char> SameLine) ContextBefore(IHunk hunk);
		/// <summary>
		/// Get information about the <see cref="LinesOfContext"/> surrounding a <paramref name="span"/> of text.
		/// </summary>
		///
		/// <param name="span">
		/// The span to get context information about.
		/// </param>
		///
		/// <returns>
		/// * <c>IndexLineBegin</c> => The number of the line where the context begins.
		/// * <c>IndexLineEnd</c> => The number of the line where the context begins.
		/// * <c>Lines</c> => The number of the line where the context begins.
		/// </returns>
		(int IndexLineBegin, int IndexLineEnd, ushort Lines) ContextInfoAfter(ReadOnlySpan<char> span)
		{
			// Create variables that will hold information about the
			ushort contextLines = 0;
			int lineContextBegin = 1;
			int lineContextEnd = 1;

			for (int i = 0; i < span.Length; ++i)
			{
				if (span[i].IsNewLine())
				{
					if (++contextLines == 1) { lineContextBegin = i; }
					if (contextLines >= this.LinesOfContext)
					{
						lineContextEnd = i;
						break;
					}
				}
			}

			return (lineContextBegin, lineContextEnd, contextLines);
		}
		/// <summary>
		/// Get information about the <see cref="LinesOfContext"/> surrounding a <paramref name="span"/> of text.
		/// </summary>
		///
		/// <param name="span">
		/// The span to get context information about.
		/// </param>
		///
		/// <returns>
		/// * <c>IndexLineBegin</c> => The number of the line where the context begins.
		/// * <c>IndexLineEnd</c> => The number of the line where the context begins.
		/// * <c>Lines</c> => The number of the line where the context begins.
		/// </returns>
		(int IndexLineBegin, int IndexLineEnd, ushort Lines) ContextInfoBefore(ReadOnlySpan<char> span)
		{
			// These are the value that will be returned, and what this method is looking for.
			ushort contextLines = 0;
			int lineContextBegin = 1;
			int lineContextEnd = 1;

			// This for loop will update the variables above whenever it comes across a newline character.
			for (int i = span.Length - 1; i >= 0; --i)
			{
				if (span[i].IsNewLine())
				{
					if (++contextLines == 1) { lineContextEnd = i; }
					if (contextLines >= this.LinesOfContext)
					{
						lineContextBegin = i;
						break;
					}
				}
			}

			return (
				IndexLineBegin: lineContextBegin,
				IndexLineEnd: lineContextEnd,
				Lines: contextLines
			);
		}
		/// <summary>
		/// Derive a diff from some <paramref name="hunks"/>.
		/// </summary>
		///
		/// <param name="hunks">
		/// The <see cref="IHunk"/> to create a diff from.
		/// </param>
		///
		/// <returns>
		/// The original text in the diff format. Contains all <see cref="IHunk"/>s from <paramref name="hunks"/> present.
		/// </returns>
		string Format(IDiff hunks);
		/// <summary>
		/// Derive a diff from some <paramref name="hunk"/>.
		/// </summary>
		///
		/// <param name="hunk">
		/// The <see cref="IHunk"/> to create a diff from.
		/// </param>
		///
		/// <returns>
		/// The original text in the diff format. Contains the <paramref name="hunk"/>.
		/// </returns>
		string Format(IHunk hunk);
		/// <summary>
		/// Include the contents of some <paramref name="from"/> <paramref name="into"/> another <see cref="IHunk"/>.
		/// </summary>
		///
		/// <param name="diffInProgress">
		/// The diff output that is being currently output.
		///
		/// </param><param name="hunk">
		/// The hunk to insert.
		///
		/// </param><param name="includedHunks">
		/// <para>The <see cref="IHunk"/>s that are already a part of <paramref name="diffInProgress"/>.</para>
		///
		/// <para>Assumed to be sorted with <see cref="IComparable{IHunk}.CompareTo(IHunk)"/></para>
		/// </param>
		///
		/// <returns>
		/// <paramref name="diffInProgress"/> after <paramref name="from"/> has been inserted.
		/// </returns>
		StringBuilder Include(StringBuilder diffInProgress, IHunk hunk, ReadOnlySpan<IHunk> includedHunks);
		/// <summary>
		/// Calculate the length of the <paramref name="hunk"/> as it exists in this format.
		/// </summary>
		///
		/// <param name="hunk">
		/// The hunk to get the length of.
		/// </param>
		///
		/// <returns>
		/// The length of <paramref name="hunk"/>.
		/// </returns>
		uint Length(IHunk hunk, uint? beforeHunkLength = null, uint? afterHunkLength = null)
		{
			hunk.Range.AsString.Length // The header
			+ (2 * (beforeHunkLength
				?? this.ContextBefore(hunk).SameLine.Length
			)) // The length from the context line to the starting index
			+ hunk.Deletion.Length // The length of the deletion from the original text
			+ hunk.Addition.Length // The length of the addition to the original text
			+ (hunk.Deletion.CountNewLines() * Environment.NewLine.Length) //
			+ (hunk.Aeletion.CountNewLines() * Environment.NewLine.Length)
			+ (2 * (afterHunkLength
				?? this.ContextAfter(hunk).SameLine.Length
			))
			+ this.LinesOfContext;
		}
		#endregion Methods
	}
}
