using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

using Iron_E.Reef.IO.Async;

namespace Iron_E.Reef.Files.VCS.Test
{
	/// <summary>
	/// Tests for <see cref="GitFactory"/>.
	/// </summary>
	public class GitTest
	{
		/// <summary>
		/// Test <see cref="GitFactory.ListFiles(string?)"/>.
		/// </summary>
		/// <param name="path">
		/// The path to use for <see cref="GitFactory.ListFiles(string?)"/>.
		/// </param>
		/// <exception cref="NullReferenceException">
		/// When <see cref="GitFactory.ListFiles(string?)"/> returns a null <c>string[]</c> value.
		/// </exception>
		[Theory]
		[InlineData("~/Programming/ACTIVE/reef")]
		public async Task ListFiles(string path)
		{
			path = path.Replace("~", Environment.GetFolderPath(
				Environment.SpecialFolder.UserProfile
			));

			Assert.True(File.Exists(
				$"{path}{Path.DirectorySeparatorChar}.gitignore"
			));

			using ProcessReader gitFactory = GitFactory.ListFiles(path);
			Task<string[]> listFiles = gitFactory.ReadAllAsync();

			// Establish the base for testing `git` commands.
			const string GIT_ARGUMENTS_DEFAULTS = "ls-files --exclude=.gitignore";

			// All of the `git` commands will use these options.
			ProcessStartInfo gitInfo = new ProcessStartInfo()
			{
				Arguments = GIT_ARGUMENTS_DEFAULTS,
				FileName = "git",
				WorkingDirectory = path
			};

			// Test using `git ls-files`
			using (ProcessReader git = new ProcessReader(gitInfo))
			{
				string[] lines = await git.ReadAllAsync().ConfigureAwait(false);
				Array.Sort(lines);
				Array.Sort(await listFiles.ConfigureAwait(false));

				Assert.True(lines.Length == listFiles.Result.Length);
				Assert.All(lines, (string line) => listFiles.Result.Contains(line));
			}

			// these commands need another argument
			const string GIT_ARGUMENTS_EXTRAS = GIT_ARGUMENTS_DEFAULTS + " --error-unmatch ";

			// This command will error if the following argument is not a path that is being tracked by git.
			foreach (string file in listFiles.Result)
			{
				// Test using `git ls-files --error-unmatch`
				using Process git = new Process() { StartInfo = gitInfo };
				// Use the base command specified, but provide the `file` for each.
				git.StartInfo.Arguments = GIT_ARGUMENTS_EXTRAS + file;

				Assert.True(
					git.Start() // make sure it starts.
					&& git.WaitForExit(10000) // make sure it stops.
					&& git.ExitCode == 0 // make sure it succeeds.
				);
			}
		}
	}
}
