using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using Text = System.Text;

using Xunit;

using Iron_E.Reef.IO.Async;
using static Iron_E.Reef.Text.DiffUtils.Extensions.StringBuilder;
using System.Text;

namespace Iron_E.Reef.Text.DiffUtils.Test
{
	/// <summary>
	/// Tests for <see cref="UnifiedDiffFormatter"/>.
	/// </summary>
	public class UnifiedDiffFormatterTest
	{
		/// <summary>
		/// Data for <see cref="UnifiedDiffFormatterTest"/>s.
		/// </summary>
		public static readonly IEnumerable<object[]> Data = new string[][]{
			new string[]{"Hello, example!", "example", "world"}
		};

		private static string[] ReadGnuDiff(string originalText, string patchedText)
		{
			/* TODO:
			 * `ProcessReader` is erroring because `gnuDiff` reports exit code 1 when two files are different.
			 * Change it to return `Task<(string[] Output, int ExitCode)>` so callers can decide what to do if an exitcode is not 0.
			 *
			 * Otherwise, this is mostly working.
			 *
			 * <20-06-10, Iron_E> */

			// Create a temporary file to store the contents of the pre-patch originalText.
			string tempFilePrePatch = $"{Path.GetTempFileName()}{Guid.NewGuid()}.reef_test";
			File.WriteAllText(tempFilePrePatch, originalText);

			// Create a temporary file to store the contents of the post-patch originalText.
			string tempFilePostPatch = $"{Path.GetTempFileName()}{Guid.NewGuid()}.reef_test";
			File.WriteAllText(tempFilePostPatch, patchedText);

			// Create a new process reader for reading output from gnu diff.
			ProcessReader gnuDiff = new ProcessReader(new ProcessStartInfo()
			{
				Arguments = $"-u {tempFilePrePatch} {tempFilePostPatch}",
				FileName = "diff"
			});

			// Return the result.
			return gnuDiff.ReadAllAsync().Result[2..]; // ← the top two lines are cut off; it is a header we don't generate.
		}

		/* TODO: Rewrite all of this shit. <2020-06-11, Iron_E> */
		/// <summary>
		/// Test for <see cref="UnifiedDiffFormatter.Format(IHunk)"/>.
		/// </summary>
		///
		/// <param name="originalText">
		/// The base text of to apply <see cref="IHunk"/>es to.
		///
		/// </param><param name="searchExpression">
		/// The search <see cref="System.Text.RegularExpressions"/> used to create <see cref="IHunk"/>es with.
		///
		/// </param><param name="replaceExpression">
		/// The pattern to replace <see cref="Text::RegularExpressions.Match"/>es of <paramref name="searchExpression"/> with.
		/// </param>
		[Theory]
		[MemberData(nameof(Data))]
		public void Format(string originalText, string searchExpression, string replaceExpression)
		{
			/* TODO: Rewrite this test. <2020-06-11, Iron_E> */
			HunkBuilder provider = new HunkBuilder(searchExpression, replaceExpression);
			Text::StringBuilder builder = new Text::StringBuilder(originalText);
			UnifiedDiffFormatter formatter = new UnifiedDiffFormatter(originalText);

			foreach (IHunk hunk in provider.Create(originalText))
			{
				// Make sure that the output from the locally-produced unified diff and gnu's diff are the same.
				Assert.Equal(
					expected: ReadGnuDiff(originalText, builder.Patch(hunk).ToString()),
					actual: formatter.Format(hunk).Split(Environment.NewLine)
				);
			}
		}

		/// <summary>
		/// Test for <see cref="UnifiedDiffFormatter.Format(IHunk)"/>.
		/// </summary>
		[Fact]
		public void FormatEnumerable()
		{
			string[][] testCases = Data.Cast<string[]>().ToArray();

			foreach (string[] args in testCases)
			{
				string originalText = args[0];

				HunkBuilder hunkBuilder = new HunkBuilder(search: args[1], replace: args[2]);
				Text::StringBuilder stringBuilder = new Text::StringBuilder(originalText);
				UnifiedDiffFormatter formatter = new UnifiedDiffFormatter(originalText);

				IEnumerable<IHunk> hunks = hunkBuilder.Create(originalText);

				Assert.Equal(
					expected: ReadGnuDiff(originalText, stringBuilder.Patch(hunks).ToString()),
					actual: formatter.Format(hunks).Split(Environment.NewLine)
				);
			}
		}
	}
}
