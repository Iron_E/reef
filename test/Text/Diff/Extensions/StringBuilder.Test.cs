using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Text = System.Text;

using Xunit;

using Iron_E.Reef.IO.Async;

namespace Iron_E.Reef.Text.DiffUtils.Extensions.Test
{
	/// <summary>
	/// Tests for <see cref="StringBuilder"/>.
	/// </summary>
	public class StringBuilderTest
	{
		private static (Text::StringBuilder Builder, IEnumerable<IHunk> Patches, HunkBuilder Provider)
			ConsumeArgs(string baseText, string search, string replace)
		{
			HunkBuilder provider = new HunkBuilder(search, replace);

			return (
				new Text::StringBuilder(baseText),
				provider.Create(baseText),
				provider
			);
		}

		/// <summary>
		/// Test for <see cref="StringBuilder.Patch(System.Text.StringBuilder, IHunk)"/>.
		/// </summary>
		///
		/// <param name="baseText">
		/// The base text of to apply <see cref="IHunk"/>es to.
		///
		/// </param><param name="search">
		/// The search <see cref="System.Text.RegularExpressions"/> used to create <see cref="IHunk"/>es with.
		///
		/// </param><param name="replace">
		/// The pattern to replace <see cref="Text::RegularExpressions.Match"/>es of <paramref name="search"/> with.
		/// </param>
		[Theory]
		[InlineData("Hello, example!", "example", "world")]
		public void ApplyPatch(string baseText, string search, string replace)
		{
			var args = ConsumeArgs(baseText, search, replace);

			foreach (IHunk hunk in args.Patches)
			{
				// Get the span for the patched text.
				string patched = args.Builder.Patch(hunk).ToString();

				// Assert that the patch is properly applied.
				Assert.Equal(
					expected: hunk.Addition,
					actual: patched[(int)hunk.Range.IndexBegin..(int)hunk.Range.IndexEndAddition]
				);

				// Get the part of the string before a patch.
				string beforeHunk(string str) => str[..(int)hunk.Range.IndexBegin];

				// Assert that the strings before the patch application are equal.
				Assert.Equal(
					expected: beforeHunk(baseText),
					actual: beforeHunk(patched)
				);

				// Assert that the strings after the patch application are equal.
				Assert.Equal(
					expected: baseText[(int)hunk.Range.IndexEndDeletion..],
					actual: patched[(int)hunk.Range.IndexEndAddition..]
				);
			}
		}
	}
}

