# About

`Reef`/`rf` (short for "refactor") is a refactoring tool, for codebases of any size, in the command line.

# Usage

You can run `Reef` with the `rf` command along with the following template:

```sh
rf [OPTIONS] SEARCH_REGEX REPLACE_REGEX [PATH]
```

## Example

```sh
rf -Cri '\(.*\)search' '\$1replacement' ./foobar
```

# Dependencies

| Name                | Version |
|:--------------------|:-------:|
| `CommandLineParser` | 2.8.0   |
| `Xunit`             | 2.4.1   |

* `git` must be in the `$PATH`/`%PATH%`.
	* If `git --help` works, you're good.
